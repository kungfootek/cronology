#!/usr/bin/perl -w use strict
#################################################################################################################
# The purpose of this perl Script is to reach out and download the NOAA sattelite image every 30 minutes 	#
# at 20 and 50 minutes past the hour. The image must then be saved in numerical sequence in order to   		#
# assemble them into one avi for the entire storm season. - I do this for fun, & yes I think I am wierd too!   	#
#################################################################################################################
# NOTE: The images being downloaded are Free and Public Domain resources provided by the US Govt.	  	#
#################################################################################################################
# 				April 13th 2010 - Daniel Baker - Unreleased, no license yet.			#
#################################################################################################################


# Plan: 
# Open Count File, Read Value and Close
# Increment Count
# Open Count File, Write Value and Close
# Create output filename
# Execute Curl and output with processed name
# Done.

# Configuration 
# This needs to be where your keeping your script
$BaseUrl="/home/blayde/2010-storm-season";
# Be sure to have a folder called "satellite-images" after your $BaseUrl !

$CountIn="$BaseUrl/NOAA-image-count.txt";
$CountOut="$BaseUrl/NOAA-image-count.txt";

open (IMPORT, $CountIn) || die "Bugger all!";
	while ($Count=<IMPORT>) 
		{ $Counted=$Count+1; }
close IMPORT;

open (EXPORT, "+>", $CountOut) || die "Bugger all!";
	print EXPORT $Counted;
close EXPORT;

$PhyleNayme=(sprintf("%06d", $Counted));
$ImageOut="$BaseUrl/satellite-images/$PhyleNayme.jpg";

system ("curl -js http://www.goes.noaa.gov/GIFS/ATIR.JPG > $ImageOut");
