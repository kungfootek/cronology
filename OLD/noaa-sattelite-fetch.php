<?php

#################################################################################################################
# The purpose of this perl Script is to reach out and download the NOAA sattelite image every 30 minutes 	#
# at 20 and 50 minutes past the hour. The image must then be saved in numerical sequence in order to   		#
# assemble them into one avi for the entire storm season. - I do this for fun, & yes I think I am wierd too!   	#
#################################################################################################################
# NOTE: The images being downloaded are Free and Public Domain resources provided by the US Govt.	  	#
#################################################################################################################
# 				April 13th 2010 - Daniel Baker - Unreleased, no license yet.			#
#################################################################################################################

/*
# Plan: 
# Open the count file and read the value
# Write the new count value
# Generate the ffmpg friendly filename
# Two reasons to use a count file;
# 1. I can remove some images to a local server if I start to run out of space
# 2. As the season wears on, counting files would make the script run longer and longer.
#
 

*/

# Configuration 
# This needs to be where your keeping your script
$BaseUrl="/home/blayde/2010_Atlantic_Storm_Season";  # No Trailing Slash!
# Be sure to have a folder called "satellite-images" after your $BaseUrl !

// Get Count from the count file
// $Counted=1;
$Counter="$BaseUrl/NOAA-image-count.txt"; $Count=0;
// print $Counter; // debug

// Read the Count
$fh = fopen($Counter, 'r') or die("can't find the count"); // fixme - Create the file if we can't find it.
$Count = fread($fh, filesize($Counter));
fclose($fh);

// Update and write the count
$fh = fopen($Counter, 'w') or die("can't open file");
$Counted=$Count+1;
fwrite($fh, $Counted);
fclose($fh);
// print $Counted; // debug

// Generate new filename
$PhyleNayme=(sprintf("%06d", $Counted));  
$ImageOut="$BaseUrl/satellite-images/$PhyleNayme.jpg";

// Grab and write the image
// print $ImageOut; // debug
    $ch = curl_init();
    $fp = fopen($ImageOut, "w");
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
    	curl_setopt($ch, CURLOPT_URL, "http://www.goes.noaa.gov/GIFS/ATIR.JPG");
    	curl_setopt($ch, CURLOPT_FILE, $fp);

    curl_exec ($ch);
    curl_close ($ch);


?>
	
