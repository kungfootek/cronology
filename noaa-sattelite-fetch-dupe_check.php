<?php
#################################################################################################################
# The purpose of this Script is to reach out and download the NOAA Atlantic sattelite image every 30 minutes 	
# The image id then saved in numerical sequence in order to assemble them into one avi for the entire storm 
# season.   	
#################################################################################################################
# NOTE: The images being downloaded are Free and Public Domain resources provided by the US Govt.	  	
#################################################################################################################
# 				April 2009 - Daniel Baker - Unreleased, no license yet.			
#################################################################################################################
#
#   The Freedom of Information Act (5 USC 552), was enacted in 1966. FOIA provides that any person has a right to 
#   obtain access to federal agency records, except to the extent that such records (or portions of them) are 
#   protected from public disclosure by one of nine FOIA exemptions or by one of three special law enforcement 
#   record exclusions. This right is enforceable in court. The Federal FOIA does not provide access to records 
#   held by state or local government agencies, or by businesses or individuals. States have their own statutes 
#   governing public access to state and local records and they should be consulted for further information about 
#   them. 
#
#
#			             C  O  P  Y  R  I  G  H  T                 N  O  T  I  C  E
#
#   		http://www.usa.gov/copyright.shtml
#
#   "What is a U.S. government work?"
# 
#   A United States government work is prepared by an officer or employee of the United States government as part 
#   of that person's official duties.
#
#   It is not subject to copyright in the United States and there are no copyright restrictions on reproduction, 
#   derivative works, distribution, performance, or display of the work. Anyone may, without restriction under 
#   U.S. copyright laws: 
#
#
#   AS SUCH IS STATED ABOVE THESE SCRIPTS ARE INVULNERABLE TO LEGAL DMCA ACTION. THE PRODUCE OF THIS APPLICATION  
#   CONSTITUTE MY DERIVITIVE WORKS. (C) ALL RIGHTS RESERVED - DANIEL BAKER - DANIEL@CLANBAKER.COM
#
#################################################################################################################

echo "\n\n- Begin Satelite Grab -\n\n";

$BaseUrl="/home/blayde/CRONOLOGY/Atlantic_Storm_Season";  # No Trailing Slash!

$CounterFileName="$BaseUrl/NOAA-image-count.txt"; $Count=0; 
$LastMD5Name="$BaseUrl/LastMD5.txt";

$GetFileCount = grab_counter($CounterFileName); 
$ImageFileName = enumerate_filename($GetFileCount, $BaseUrl);


## We really haven't Voided main(); in a long time..

## This while loop is making sure the image retrieved is not a duplicate, and that is is at least 70k in size.
## A counter needs to be added to abort the while if it exceeds (x) iterations, a division of 30 minutes by sleep time.
## No more than 20 iterations, so one script doesn't take longer than 30 minutes to run, and cause a race condition.

while ($LastMD5 == $CurrentMD5 and $Count <= 15)
{
	echo "\nCount : $Count\n";
	$RetrieveImageToDisk = get_latest_satellite($ImageFileName);
	$LastMD5=Get_Last_MD5($LastMD5Name);
	$CurrentMD5 = md5_file($ImageFileName);

	echo "Old MD5 : $LastMD5\n";
	echo "New MD% : $CurrentMD5 \n";
	
	$Count++;

	
}


if ($LastMD5 != $CurrentMD5)
	{
		$ImageNumber = (sprintf("%06d", $GetFileCount));
		rename($ImageFileName, "$BaseUrl/satellite-images/$ImageNumber.jpg");
		counter_write_increment($CounterFileName, $GetFileCount);
		Push_Last_MD5($LastMD5Name, $CurrentMD5);
	}
	
echo "\n\n- Satelite Grab Complete - \n\n";

exit;	
function Get_Last_MD5($LastMD5Name)
{
	$LastMD5 = file_get_contents($LastMD5Name);
return $LastMD5;
}

function Push_Last_MD5($LastMD5Name, $CurrentMD5)
{
	$fh = fopen($LastMD5Name, 'w') or die("can't open file");
	fwrite($fh, $CurrentMD5); fclose($fh);
	echo "Wrote : $CurrentMD5\n";
return 0;
}
	
function grab_counter($CounterFileName)
{
	$Count = file_get_contents($CounterFileName);
return $Count;
}

function counter_write_increment($CounterFileName, $GetFileCount)
{ 	
	$GetFileCount=$GetFileCount+1;
	$fh = fopen($CounterFileName, 'w') or die("can't open file");
	fwrite($fh, $GetFileCount); fclose($fh);
return 0;
}

function enumerate_filename($Counted, $BaseUrl)
{
 	$PhyleNayme=(sprintf("%06d", $Counted));  
	$ImageFileName="$BaseUrl/satellite-images/temp/$PhyleNayme.jpg";
return $ImageFileName;
}

function get_latest_satellite($ImageFileName)
{ 

	while ( $filesize <= 70000) 
	{
		$ch = curl_init();
	   	$fp = fopen($ImageFileName, "w");
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		curl_setopt($ch, CURLOPT_URL, "http://www.goes.noaa.gov/GIFS/ATIR.JPG");
	  	curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_exec ($ch);
	   	curl_close ($ch);

   		# Don't WebFlood NOAA Servers and give the file time to finish downloading
		sleep(75);

		clearstatcache();
   		$filesize=filesize($ImageFileName);
		echo "\nFilesize: $filesize at : ";	
   		}
   	echo "$ImageFileName\n";
   	
return 0;
}


?>
	
